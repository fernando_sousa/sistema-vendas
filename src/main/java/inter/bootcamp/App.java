package inter.bootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBoot$;


@SpringBootApplication
public class App {

        public static void main(String[] args) {
                SpringApplication.run(App.class, args);
        }

}
